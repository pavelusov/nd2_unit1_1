/**
 * Created by pavelusov on 09.10.16.
 */
'use strict';
// 1 часть 1 пункт
class Pokemon {
    constructor(name, level){
        this.name = name;
        this.level = level;
    }
    show () {
      console.log(`${this.name}, уровень ${this.level}`); // По фэншую!
    }
}
// let poc1 = new Pokemon("Artur", 50);
// let poc2 = new Pokemon("Pavel", 40);
// poc1.show();
// poc2.show();

// 1 часть 2 пункт
 class  PokemonList extends Array{ 
     constructor(...pokemons){ 
        pokemons = pokemons.filter(obj => obj instanceof Pokemon); // Используем функцию стрелку
        super(...pokemons);    // вызываем конструктор от Array
     } 
     add (name, level) {          
            this.push(new Pokemon(name, level));
     } 
     show (){ // 2 часть 3 пункт
         console.log(`Список покемонов. Количество ${this.length}`);
         this.forEach(pokemon => pokemon.show());
     }

}

// 2 часть 1 пункт
let lost = new PokemonList(new Pokemon("Makar", 455), new Pokemon("Bart", 44));
let found = new PokemonList(new Pokemon("Marmot", 66), new Pokemon("Burbazavr", 666) );
// 2 часть 2 пункт
lost.add("Krakomon", 33);
lost.add("Klavomoon", 378);
found.add("Partook", 86);
found.add("Korkook", 55);
found.add("Tortoz", 335);
lost.show();
found.show();
// 2 часть 4 пункт
console.log("## transfer ##");
// console.log(lost); // Каждый элемент в массиве lost это экземпляр класса Pokemon.
// console.log(...lost); // разбиваем массив с помощью оператора деструктуризации 58 страница.
found.push(...lost.splice(1,1));
lost.show();
found.show();